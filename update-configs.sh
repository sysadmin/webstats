#!/bin/bash
# Comment the below to enable output
exec &> /dev/null

logfiles=`ls /var/log/apache2/*.log | grep -v error.log`

rm /etc/awstats/*.conf
for logfile in $logfiles; do 
	stripsuffix=${logfile%.log}
	site=${stripsuffix#*apache2\/}
	if [ -h /etc/apache2/sites-enabled/$site ]; then
		echo "Processing $site"

		echo "LogFile=\"$logfile\"" > /etc/awstats/awstats.$site.conf
		echo "SiteDomain=\"$site\"" >> /etc/awstats/awstats.$site.conf

		aliases=`grep -i serveralias /etc/apache2/sites-enabled/$site`
		aliaslist=$site
		for alias in $aliases; do
			if [ "$alias" != "ServerAlias" ]; then 
				echo "With alias: "$alias
				aliaslist=$aliaslist" "$alias
			fi
		done
		echo "HostAliases=\"$aliaslist\"" >> /etc/awstats/awstats.$site.conf
		echo "Include \"/srv/awstats/awstats-template.conf\"" >>  /etc/awstats/awstats.$site.conf
	else
		echo "ERROR processing $site, no apache configuration exists"
	fi
done
