#!/bin/bash
exec &> /dev/null

for conffile in $(ls /etc/awstats/awstats.*.conf); do
    conffile=${conffile#/etc/awstats/awstats.}
    conffile=${conffile%.conf}
    /usr/lib/cgi-bin/awstats.pl -config=$conffile -update
done
