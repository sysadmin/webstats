#!/bin/bash
exec &> /dev/null

echo "<h1>List of Website statistics available:</h1>" > /var/www/stats/index.html

# Debian and Ubuntu are inconsistent....
if [ -e /usr/share/doc/awstats/examples/awstats_buildstaticpages.pl ]; then
    BUILDSCRIPT=/usr/share/doc/awstats/examples/awstats_buildstaticpages.pl
elif [ -e /usr/share/awstats/tools/awstats_buildstaticpages.pl ]; then
    BUILDSCRIPT=/usr/share/awstats/tools/awstats_buildstaticpages.pl
else
    echo "awstats_buildstaticpages.pl not available!"
    exit 1
fi

for conffile in $(ls /etc/awstats/awstats.*.conf); do
    conffile=${conffile#/etc/awstats/awstats.}
    conffile=${conffile%.conf}
    $BUILDSCRIPT -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/var/www/stats/ -config=$conffile
    echo "<a href='/stats/awstats.$conffile.html'>$conffile stats</a><br/>" >> /var/www/stats/index.html
done
